package tn.esprit.test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Book;
import tn.esprit.entities.User;
import tn.esprit.services.RemotUser;
import tn.esprit.services.RemoteBook;

public class ClientMain {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		
		Context ctx = new InitialContext();
		
		RemotUser remotUser = (RemotUser) ctx.lookup("Revision-ear/Revision-ejb/UserService!tn.esprit.services.RemotUser");
		
		RemoteBook remoteBook = (RemoteBook) ctx.lookup("Revision-ear/Revision-ejb/BookService!tn.esprit.services.RemoteBook");
		
		User u = new User();
		u.setLogin("ahmed");
		u.setPassword("esprit");
		u.setEmail("ahmed@esprit.tn");
		
		Book b = new Book();
		b.setDescription("Computer Sciences");
		b.setTitle("LNAI");
		b.setPrice((float) 60);
		b.setNbOfpage(100);
		b.setIllustrations(false);
		
		Book b1 = new Book();
		b1.setDescription("Livre de NLP");
		b1.setTitle("Pouvoir illimit�");
		b1.setPrice((float) 20);
		b1.setNbOfpage(393);
		b1.setIllustrations(true);
		
		remoteBook.addBook(b);
		remoteBook.addBook(b1);
		remotUser.addUser(u);

	}

}
